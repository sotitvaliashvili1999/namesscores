import { namesArray } from "./namesArray.js";

function namesScores(array) {
  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let totalScore = 0;
  array.sort();
  for (let i = 0; i < array.length; i++) {
    let nameWorth = 0;
    for (let j = 0; j < array[i].length; j++) {
      nameWorth += alphabet.indexOf(array[i][j]) + 1;
    }
    totalScore += nameWorth * (i + 1);
  }
  return totalScore;
}

console.log(namesScores(namesArray));
